import os
from flask import Flask
from flask import jsonify
from flask import request
from flask_cors import CORS

import matplotlib.pyplot as plt
import numpy as np

from keras.applications.resnet import ResNet50
from keras.preprocessing import image
from keras.applications.resnet import preprocess_input, decode_predictions
from keras.applications.vgg16 import preprocess_input
from keras.models import load_model

app = Flask(__name__)
CORS(app)
app.config['MAX_CONTENT_LENGTH'] = 1024 * 1024
app.config['UPLOAD_EXTENSIONS'] = ['.jpg', '.png', '.gif']
app.config['JSONIFY_PRETTYPRINT_REGULAR'] = False

# Dictionary for convert class number to text
class2text = {
    0: 'Chihuahua',
    1: 'Golden Retriever',
    2: 'Siberian Husky',
    3: 'Pomeranian',
    4: 'Pug',
}

@app.route('/ping', methods=['GET'])
def pong():
    return "pong!"

@app.route('/classify', methods=['POST'])
def classify():
    uploaded = request.files['img_file']
    filename = uploaded.filename
    print("Uploaded file:", filename)

    if filename != '':
        file_ext = os.path.splitext(filename)[1]
        if file_ext not in app.config['UPLOAD_EXTENSIONS']:
            return jsonify({
            'message': 'uploaded file extension not support.',
        }), 400

        uploaded.save(filename)
        

    # test file
    # img = image.load_img(uploaded.filename)
    # array = image.img_to_array(img)
    # plt.imshow(array.astype('uint8'))
    # plt.show()
    # plt.close("all")
    # print(f"Image shape: {array.shape}")

    is_dog = dog_detector(filename)
    # is_dog == false -> return
    if not is_dog:
        return jsonify({
            'message': 'Hmmm... This doesn\'t look like a dog.',
        }), 200
    
    # predict dog breed
    predict_class, prob = classify_dog(filename)

    # delete image
    if os.path.exists(filename):
        os.remove(filename)

    return jsonify(
        {
            'file_name': filename,
            'class': class2text[predict_class],
            'prob': str(prob)
        }
    ), 200

# reshape input to shape (1, 224, 224, 3)
def resize_input(img_path, width, height):
  # Load and resize the image
  img = image.load_img(img_path, target_size=(width, height))
  # convert PIL.Image.Image type to 3D tensor with shape (224, 224, 3)
  img = image.img_to_array(img)
  # convert 3D tensor to 4D tensor with shape (1, 224, 224, 3) and return 4D tensor
  img = np.expand_dims(img, axis=0)
  # img = np.asarray(img)
  return img

### returns "True" if a dog is detected in the image stored at img_path
def dog_detector(img_path):
    # reshape the input
    img = resize_input(img_path, 224, 224)

    # Preprocess the input array
    x = preprocess_input(img)

    # weights = 'imagenet' means that we will use the ResNet50
    # that has been pretrained on ImageNet
    model = ResNet50(weights='imagenet')

    # Feed the preprocessed, downloaded image to the pretrained ResNet50.
    # The outputs are the probabilities of classes defined in ImageNet.
    probs = model.predict(x)

    # Convert the label back to the original format - get max prob predict class
    prob_classes = np.argmax(probs)

    return ((prob_classes <= 268) & (prob_classes >= 151))

def classify_dog(filename):
    images = []
    # Load and resize the image
    img = image.load_img(filename, target_size=(224, 224))

    # Convert the Image object into a numpy array
    img = image.img_to_array(img)

    # Add to a list of images
    images.append(img)
    images = np.asarray(images)

    # Preprocess the input array
    x = preprocess_input(images)

    # load save model
    model = load_model('tbd_model.h5')

    # predict the class
    probs = model.predict(x)

    # Convert the label back to the original format
    prob_classes = np.argmax(probs, axis=-1)
    max_prob_class = prob_classes[0]

    result = list(probs[0])
    percent = f'{result[max_prob_class]*100:.2f}'

    return max_prob_class, percent
    
if __name__ == '__main__':
    app.run(host='localhost', port=8000)